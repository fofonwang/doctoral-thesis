\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{xcolor}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Effect of Light On Water Balance Through Gas Exchange},
            pdfauthor={Fang Wang},
            colorlinks=true,
            linkcolor=Maroon,
            filecolor=Maroon,
            citecolor=Blue,
            urlcolor=blue,
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage[margin=1in]{geometry}
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\usepackage{tabularx}
\usepackage{hyperref}
\usepackage{FWAbbrev}

\title{Effect of Light On Water Balance Through Gas Exchange}
\author{Fang Wang}
\date{January 2020}

\begin{document}
\maketitle

\hypertarget{summary}{%
\section{Summary}\label{summary}}

Plants must maintain a balanced water budget to support fundamental
processes, such as carbon assimilation, nutrient transport, growth and
energy storage, and in the end to survive and reproduce. Light, on the
other hand, is the ultimate energy source and therefore vital in the
plants' energy balance. Most processes that allow light to be utilised
also require water use, creating a conflict for plants that need to
minimise water loss, for instance when water is scarce. To use light or
to use water represents a trade-off that persists throughout a plant's
life, but is especially important during the establishment of a
seedling.

In this thesis work, I conclude that: Light can affect plant water
relations by regulating the rate and magnitude of stomatal movement (I);
Blue-light photoreceptors play important roles in this movement under
blue and green light, with their contributions to stomatal control
changing though the photoperiod (I); When plants grow in shade, the
response of stomatal conductance to long-term water-deficit may become
more conservative (III); Under drought, related species can have
different water use strategies, including the regulation of stomatal
conductance (II). Further research will be required to understand the
roles of photoreceptors in these strategies. These findings may become
useful tools in manipulation of light quality (\textit{e.g.} blue:PAR
photon ratio) to regulate water relations and achieve better growth in
indoor horticulture where LEDs are widely utilised. However, much
remains to be done. The roles of photoreceptors in diurnal regulation of
water relations during a drought period are far from clear. Practical
applications of light quality and intensity for manipulation of the
trade-off between high water use efficiency and low hydraulic risk also
require further study.

\hypertarget{list-of-original-publications-and-manuscripts}{%
\section{List of Original Publications and
Manuscripts}\label{list-of-original-publications-and-manuscripts}}

\begin{table}[ht]
\centering
\begin{tabularx}{\textwidth}{lX}
I   & \textbf{Wang F}, Robson TM, Casal JJ, Shapiguzov A, Aphalo PJ (In press) Contributions of cryptochromes and phototropins to stomatal opening through the day. Functional Plant Biology \\ 
II  & Randriamanana T, \textbf{Wang F}, Lehto T, Aphalo PJ (2012) Water use strategies of seedlings of three Malagasy Adansonia species under drought. South African Journal of Botany 81:61–70. \href{https://doi.org/10.1016/j.sajb.2012.05.005}{doi}\\ 
III & \textbf{Wang F}, Israel D, Ram\'irez-Valiente J-A, S\'anchez-G\'omez D, Aranda I, Aphalo PJ, Robson TM (manuscript) Seedlings from marginal and core populations of European beech (\textit{Fagus sylvatica} L.) respond differently to imposed drought and shade \\ 
IV & \textbf{Wang F} (2016) Tutorial: SIOX plugin in ImageJ: area measurement made easy. UVPlants Bulletin 2016:37–44.  \href{https://doi.org/10.19232/uv4pb.2016.2.11}{doi}
\end{tabularx}
\end{table}

\end{document}
