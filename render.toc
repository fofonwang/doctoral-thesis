\contentsline {chapter}{DEDICATION}{iii}{chapter*.1}
\contentsline {chapter}{ACKNOWLEDGEMENTS}{v}{chapter*.2}
\contentsline {chapter}{ABSTRACT}{vii}{chapter*.3}
\contentsline {chapter}{ORIGINAL PUBLICATIONS}{1}{chapter*.5}
\contentsline {section}{List of Original Publications and Manuscripts}{1}{section*.6}
\contentsline {section}{Author contributions}{2}{table.caption.7}
\contentsline {chapter}{ABBREVIATIONS}{3}{chapter*.9}
\contentsline {chapter}{\numberline {1}INTRODUCTION}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Water relations}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Acclimation and adaptation}{7}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Water potential}{7}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Leaf gas exchange}{8}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Light}{8}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Perception of light}{10}{subsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.1.1}Photoreceptors}{12}{subsubsection.1.2.1.1}
\contentsline {subsection}{\numberline {1.2.2}Blue light (BL) and the two main BL-photoreceptors}{12}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Shade effect}{14}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Leaf stomatal function}{14}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Stomatal opening driven by light}{14}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Stomatal optimisation and WUE}{15}{subsection.1.3.2}
\contentsline {chapter}{\numberline {2}OBJECTIVES}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Objectives and hypotheses}{17}{section.2.1}
\contentsline {chapter}{\numberline {3}MATERIALS AND METHODS}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Materials}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Species}{19}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Morphology}{19}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Methods}{19}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Treatments and devices}{19}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Statistics}{23}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}R packages}{23}{subsubsection.3.2.2.1}
\contentsline {chapter}{\numberline {4}RESULTS AND DISCUSSION}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Quick or slow stomatal opening}{26}{section.4.1}
\contentsline {section}{\numberline {4.2}Environmental effects on the degree of stomatal opening}{27}{section.4.2}
\contentsline {section}{\numberline {4.3}Diurnal patterns in stomatal opening}{29}{section.4.3}
\contentsline {section}{\numberline {4.4}Acclimation and adaptation to drought and shade}{29}{section.4.4}
\contentsline {section}{\numberline {4.5}Reproducibility in research work}{30}{section.4.5}
\contentsline {chapter}{\numberline {5}CONCLUSIONS}{33}{chapter.5}
\contentsline {chapter}{REFERENCES}{35}{chapter*.19}
\contentsline {chapter}{INDEX}{49}{chapter*.19}
