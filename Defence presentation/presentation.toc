\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{BACKGROUND}{3}{0}{1}
\beamer@sectionintoc {2}{OBJECTIVES AND HYPOTHESES}{7}{0}{2}
\beamer@sectionintoc {3}{MATERIALS AND METHODS}{9}{0}{3}
\beamer@sectionintoc {4}{RESULTS}{11}{0}{4}
\beamer@sectionintoc {5}{CONCLUSIONS}{15}{0}{5}
