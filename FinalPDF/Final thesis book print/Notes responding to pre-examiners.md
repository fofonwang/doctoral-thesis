# Vladimir

Minor formal comments/suggestions for improvement of Thesis (the annotated PDF is an appendix)

Few typos are marked directly in the Thesis.
**Checked**

The links to DOI on page 7 (section Original publications) does not work. lt would be safer to write the entire DOl. As
paper Wang et al. has been already published in Functional Plant Biology, citation should be completed (including DOI).
**Done**

Figures/Legend of figures
A style of referring to figures in text should be unified (Fig. is prevailing but at least on page 6 Figure 1. 1. ).
Fig. 1.5. is not mentioned in tne text - lt should be referred to, otheruvise it is not clear why it is presented. Similarly Figs.
3.2. and 3.3. (l have not seen the references to these Figs. In the text.
Fig. on page 13 is not numbered, nor legend is presented (it is referred to as Figure on next page.
Fang: Now the figure is numbered. Figure 1.6: Spectral absorbance of CRY1, CRY2 and PHOT1 (Banerjee et al., 2007; Zeugner et al., 2005). But PHOT1 ref is lacking.


Generai comments: Although the other figures have a legend, it is according to my opinion too brief (it refers to published
papers). I would suggest that figure legends in the study should be more self-explanatory.
References:
The use of capitals should be unified in the titles of cited articles (only the first capital letter prevails but in some cases
"US style" is used). The use of capitals in journal titles should be unified as well.
Some of the references are (or seem to be) incomplete (marked directly in Thesis, check list of the refbrences):
A: Source Journal (and corresponding information, eg. vol. pages, doi) is missing. This may be the case also for some
of the references to the R packages (listed in the section Material and Methods, on the pages 23-24) and referred
throughout the text of the study) - if these references were not published in per review journals a note should be added
at the end of corresponding citations in the list of References informing that this is a special type of literature reference.
Othenruise it looks incomplete, too.
B: Instead of year of issue some citations contain n.d. (both in the text and list). However, all papers were already
published, so this should be actualized both in text and list of references (including full citation).
According to my opinion it would be better if Table 3.2. will be on one page.
Although I have declared that Chapter Results and discussion is well written, my opinion is that tn few cases it should be
more self-explanatory, as it the reader has to go to the Publications in order to fully understand the message. This is
particularly valid for subchapter 4.4 (page 30). Author write: "When well-watered, the beech population Montejo-ES from
the Mediteranean region had the largest difference in Gs between the light and shed when compared to populations of
other origins (lll)." I would suggest that before this statement, beech populations studied in this manuscript should be
briefly described regarding the environments where plants have evolved, in order to clearly explain the dependence